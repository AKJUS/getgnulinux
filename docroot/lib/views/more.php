<?php

# This file is not a valid entry point; stop processing unless GGL is defined.
if ( !defined('GGL') ) {
    exit(1);
}

$this->load_header();

?>

<main>
  <article>
    <div class="container">
      <header>
        <h1><?php print _("More"); ?></h1>
        <div class="page-intro">
          <p><?php print _("Selected reading on the Internet."); ?></p>
        </div>
      </header>

      <h2 class="center"><?php print _("Questions and help"); ?></h2>

      <br>

      <div class="row">
        <div class="col s12 l6 pad3">
          <div class="center">
            <a href="https://unix.stackexchange.com/" title="Unix &amp; Linux Stack Exchange" target="_blank">
              <img class="lift-effect" src="/static/images/more/unix.stackexchange.png" width="280" alt="Unix &amp; Linux Stack Exchange">
            </a>
          </div>
          <p><?php print _("<strong>Unix &amp; Linux Stack Exchange</strong> is a question and answer site for users of GNU/Linux. This site is all about getting answers."); ?></p>
        </div>
        <div class="col s12 l6 pad3">
          <div class="center">
            <a href="https://askubuntu.com/" title="Ask Ubuntu" target="_blank">
              <img class="lift-effect" src="/static/images/more/askubuntu.png" width="280" alt="Ask Ubuntu">
            </a>
          </div>
          <p><?php print _("<strong>Ask Ubuntu</strong> is a question and answer site for Ubuntu users. Even for users of different distributions of GNU/Linux this can be a helpful resource for finding answers."); ?></p>
        </div>
        <div class="col s12 l6 pad3">
          <div class="center">
            <a href="https://ask.fedoraproject.org/" title="Ask Fedora" target="_blank">
              <img class="lift-effect" src="/static/images/more/askfedora.png" width="280" alt="Ask Fedora">
            </a>
          </div>
          <p><?php print _("<strong>Ask Fedora</strong> is a forum for Fedora users. Here you can speak to the community to get help with installing, using, customizing, and upgrading a system running Fedora."); ?></p>
        </div>
      </div>

      <div class="flex-row">
        <div class="flex-col flex-col--w350">
          <div class="card amber lighten-4">
            <div class="card-content">
              <span class="card-title"><?php print _("Internet Relay Chat (IRC)"); ?></span>
              <p><?php print _("There are many real-time discussion channels for the free software community. IRC, or Internet Relay Chat, is a real-time, text-based form of communication. You can have conversations with multiple people in an open channel or chat with someone privately one-on-one. Major GNU/Linux distributions have their own dedicated IRC channels where you'll find users and developers that are happy to answer your questions. Here you can find the IRC channels for the distributions we recommend:"); ?></p>
              <ul class="browser-default">
                <li><a href="https://wiki.debian.org/IRC" rel="external">Debian</a></li>
                <li><a href="https://wiki.ubuntu.com/IRC/ChannelList" rel="external">Ubuntu</a></li>
                <li><a href="https://fedoraproject.org/wiki/Communicating_and_getting_help#IRC" rel="external">Fedora</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="flex-col flex-col--w350">
          <div class="card amber lighten-4">
            <div class="card-content">
              <span class="card-title"><?php print _("Mailing lists"); ?></span>
              <p><?php print _("All major GNU/Linux distributions offer help &ndash; free of charge &ndash; through mailing lists:"); ?></p>
              <ul class="browser-default">
                <li><a href="https://www.debian.org/MailingLists/" rel="external">Debian</a></li>
                <li><a href="https://lists.ubuntu.com/" rel="external">Ubuntu</a></li>
                <li><a href="https://lists.fedoraproject.org/archives/" rel="external">Fedora</a></li>
              </ul>
            </div>
          </div>
          <div class="card amber lighten-4">
            <div class="card-content">
              <span class="card-title"><?php print _("Commercial support"); ?></span>
              <p><?php print _("If you need someone you can reach on the phone anytime to assist you, the companies behind most distributions provide commercial support: see <a href=\"https://www.debian.org/consultants/\" rel=\"external\">Debian consultants</a>, <a href=\"https://ubuntu.com/support\" rel=\"external\">Ubuntu Commercial Support</a>, or <a href=\"https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux\" rel=\"external\">Red Hat Enterprise Linux</a> for example."); ?></p>
            </div>
          </div>
        </div>
      </div>

      <h2 class="center"><?php print _("Learn and read more"); ?></h2>

      <br>

      <div class="row">
        <div class="col s12 l6 pad3">
          <div class="center">
            <a href="https://en.wikibooks.org/wiki/FLOSS_Concept_Booklet" title="The FLOSS concept booklet" target="_blank">
              <img class="lift-effect" src="/static/images/more/floss-booklet.png" width="299" alt="FLOSS Booklet">
            </a>
          </div>
          <p><?php print _("A short, entertaining way to clarify thoughts about free, libre, \"open-source\" software."); ?></p>
        </div>
        <div class="col s12 l6 pad3">
          <div class="center">
            <a href="https://www.fsf.org/" title="Free Software Foundation" target="_blank">
              <img class="lift-effect" src="/static/images/more/fsf.png" width="299" alt="FSF">
            </a>
          </div>
          <p><?php print _("The Free Software Foundation defends and advocates the very concept of <a href=\"https://www.gnu.org/philosophy/free-sw.html\" rel=\"external\">free software</a> that gave birth to GNU/Linux."); ?></p>
        </div>
      </div>

      <br>

      <p class="note center"><?php print _("Note: these links are proposed as a recommendation. They are not commercial."); ?></p>

      <br>
    </div>
  </article>
</main>

<?php $this->load_footer(); ?>
