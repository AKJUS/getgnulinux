import $ from 'jquery';

import {initTheme, toggleTheme, updateThemeButton} from './theme';

import './styles/main.scss';


window.jQuery = $;

const theme = initTheme();

window.addEventListener('DOMContentLoaded', () => {
    const button = document.querySelector('.dark-mode-button');

    if (button !== null) {
        button.addEventListener('click', function(e) {
            e.preventDefault();
            toggleTheme(true);
        });
    }

    updateThemeButton(theme);

    $('.sidenav').sidenav();
    $('.language-button-small').dropdown();
    $('.language-button-large').dropdown();
    $('.materialboxed').materialbox();

    const flexslider = document.querySelector('.flexslider');

    if (flexslider !== null) {
        $('.flexslider').flexslider({
            animation: 'fade',
        });
    }
});
